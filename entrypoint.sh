#!/bin/bash
set -eu

# if wordpress already installed
if [ -f wp-load.php ]; then
    echo >&2 "wp-load.php is already present. Database will not be overridden and files are kept as is."

elif [[ "$1" == apache2* ]] || [ "$1" == php-fpm ]; then

	# if we already have wp-config.php, we will rename it temporarily
	if [ -f wp-config.php ]; then
		mv wp-config.php wp-config-temp.php
	fi

	sleep 20
	# Set default values
	: ${WP_VERSION:=latest}
	: ${WP_DOMAIN:=localhost}
	: ${WP_URL:=http://localhost}
	: ${WP_LOCALE:=en_US}
	: ${WP_SITE_TITLE:=WordPress for development}
	: ${WP_ADMIN_USER:=admin}
	: ${WP_ADMIN_PASSWORD:=admin}
	: ${WP_ADMIN_EMAIL:=admin@example.com}

	: ${WP_DB_HOST:=mysql}
	: ${WP_DB_PREFIX_TABLE:=wp_}
	: ${WP_DB_USER:=root}
	: ${WP_DB_PASSWORD:=root}
	: ${WP_DB_NAME:=wordpress}

	if [ -z "$WP_DB_PASSWORD" ]; then
		echo >&2 'error: missing required WP_DB_PASSWORD environment variable'
		echo >&2 '  Did you forget to -e WP_DB_PASSWORD=... ?'
		echo >&2
		echo >&2 '  (Also of interest might be WP_DB_USER and WORDPRESS_DB_NAME.)'
		exit 1
	fi

	wp cli --allow-root update --nightly --yes

	# Download WordPress.
	wp core --allow-root download \
		--version=${WP_VERSION} \
		--locale=${WP_LOCALE} \
		--force --debug

	# Generate the wp-config file for debugging.
	wp core --allow-root config \
		--dbhost="$WP_DB_HOST" \
		--dbname="$WP_DB_NAME" \
		--dbuser="$WP_DB_USER" \
		--dbpass="$WP_DB_PASSWORD" \
		--dbprefix="$WP_DB_PREFIX_TABLE" \
		--locale="$WP_LOCALE" \
		--extra-php <<PHP
// Load composer autoloader.
if ( file_exists( ABSPATH . '/vendor/autoload.php' ) ) {
	require_once( ABSPATH . '/vendor/autoload.php' );
}
PHP

	# Install WordPress.
	wp core --allow-root install \
		--url="${WP_URL}" \
		--title="${WP_SITE_TITLE}" \
		--admin_user="${WP_ADMIN_USER}" \
		--admin_password="${WP_ADMIN_PASSWORD}" \
		--admin_email="${WP_ADMIN_EMAIL}" \
		--skip-email

	# Delete Pligins.
	wp plugin --allow-root delete $(wp plugin --allow-root list --field=name)

	# after installation rename temporarily wp-config-temp.php to wp-config.php
	if [ -f wp-config-temp.php ]; then
		mv wp-config-temp.php wp-config.php
	fi

	# Delete readme.html
	rm readme.html

	# Delete wp-config-sample.php
	rm wp-config-sample.php

	# Add domain to hosts file. Required for Boot2Docker.
	# echo "127.0.0.1 ${WP_DOMAIN}" >> /etc/hosts

	echo >&2 "Access the WordPress admin panel here ${WP_URL}"
fi

exec "$@"