
# Образ Wordpress Docker

## Для того, чтобы отправить образ на Docker Hub нужно:

    Зайти на https://hub.docker.com/ и создать репозитории

### Собрать образ

    $ docker build -t <название образа>:<тэг> --file <путь до Dockerfile> .

    docker build -t wordpress --file php7.2/Dockerfile .
    или
    docker build -t wordpress:php7.2 --file php7.2/Dockerfile .

### Залогиниться

    $ docker login --username=yourhubusername

### Добавить тег

    $ docker tag <название-образа/IMAGE ID> <hub-user>/<repo-name>:<tag>

    пример: $ docker tag wordpressdocker_wordpress(или ce57d4b85c65) drakeps/wordpress:latest

### Запушить

    $ docker push <hub-user>/<repo-name>:<tag>

    пример: $ docker push drakeps/wordpress:latest
